<?php
include './head.php';
?>

<body>
    <?php
    include './navbar.php';
    $id = isset($_POST['edit-bookmark-id']) ? $_POST['edit-bookmark-id'] : null;
    $name = isset($_POST['edit-bookmark-name']) ? $_POST['edit-bookmark-name'] : null;
    $url = isset($_POST['edit-bookmark-url']) ? $_POST['edit-bookmark-url'] : null;

    ?>
    <div class="container">
        <h1 class="title my-6">Page de modification du favoris <span class="favoris-name"><?= $name ?></span></h1>
        <form class="edit-form box" action="../crud/update.php" method="POST">
            <div class="field">
                <label class="label" for="edit-id">ID du Favoris</label>
                <div class="control">
                    <input class="input is-static is-rounded" type="text" name="edit-id" id="edit-id" value="<?= $id ?>" readonly>
                </div>
            </div>
            <div class="field">
                <label class="label" for="name">Nom du Favoris*</label>
                <div class="control">
                    <input class="input is-rounded" type="text" name="edit-name" id="edit-name" value="<?= $name ?>">
                </div>
            </div>
            <div class="field">
                <label class="label" for="url">URL du Favoris*</label>
                <div class="control">
                    <input class="input is-rounded" type="text" name="edit-url" id="edit-url" value="<?= $url ?>">
                </div>
            </div>
            <div class="block">
                <div class="field">
                    <label class="label" for="bookmark-categories">Dans quelle catégorie enregistrer ce nouveau lien ?</label>
                    <div class="control">
                        <?php
                        require '../crud/read.php';
                        $bookmarksCat = Read::getOneBookmarkWithCategories($id);
                        $allCategories = Read::getCategories();
                        $rows = $bookmarksCat->fetchAll(PDO::FETCH_ASSOC);
                        $results = $allCategories->fetchAll(PDO::FETCH_ASSOC);
                        foreach($results as $key => $result) {
                            echo '
                                <label class="checkbox mr-5" for="' . $result['id'] . '">
                                    <input name="id_categories[]" type="checkbox" value="' . $result['id'] . '" id="' . $result['id'] . '" ';
                                    foreach($rows as $key => $row) {
                                        if($result['id'] == $row['id']) {
                                            echo "checked";
                                        }
                                    }
                                    echo '/>
                                    ' . $result['name'] . '
                                </label>
                            ';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="field my-5">
                <div class="control">
                    <button class="button is-primary is-rounded" type="submit">Modifier le favoris</button>
                </div>
            </div>
        </form>
        <div>
            <a class="button is-danger is-rounded my-6" href="/index.php?page=liste&type=favoris">Retour à la liste des favoris</a>
        </div>
    </div>
    <?php include './footer.php'; ?>
</body>

</html>