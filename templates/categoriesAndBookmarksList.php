<?php
$getAllCategories = Read::getCategories();
?>
<h1 class="title mb-6">Classement par Catégories</h1>
<div class="columns is-flex-wrap-wrap">
    <?php
    $results = $getAllCategories->fetchAll(PDO::FETCH_ASSOC);
    foreach ($results as $key => $result) {
        $bookmarksInCategory = Read::getCategoriesAndHerBookmarks($result['id']);

        if (empty($result['description'])) {
            $desc_tooltip = "";
        } else {
            $desc_tooltip = 'data-tooltip="' . $result['description'] . '"';
        }
        $count = $bookmarksInCategory->rowCount();
        $rows = $bookmarksInCategory->fetchAll(PDO::FETCH_ASSOC);
        if ($count > 0): ?>
            <div class="column is-6">
                <div class="category-block box">
                    <h3 class="category-name">
                        <span class="span-category-name" <?= $desc_tooltip ?>>
                            <?= $result['name'] ?>
                        </span> 
                        <span class="tag is-dark">
                            <?= $count ?>
                        </span>
                    </h3>

                    <?php foreach ($rows as $key => $row): ?>
                    <p class="p-bookmark-name ml-6"><a href="<?= $row['url'] ?>" data-tooltip="<?= $row['url'] ?>" target="_blank"><?= $row['name'] ?></a></p>
                    <?php endforeach; ?>

                </div>
            </div>
        <?php endif; ?>
    <?php } ?>
</div>