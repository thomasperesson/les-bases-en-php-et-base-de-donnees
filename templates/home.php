<h1 class="title has-text-centered">Gestions d'une liste de favoris</h1>

<div class="columns is-centered">
    <div class="column is-half">
        <div class="card my-5">
            <div class="card-content">
                <p class="title is-4">
                    Afficher la liste des Favoris ou la liste des Catégories
                </p>
            </div>
            <footer class="card-footer">
                <p class="card-footer-item">
                    <span>
                        Afficher la liste des
                        <a class="has-text-weight-bold" href="./index.php?page=liste&type=favoris"
                            >Favoris</a
                        >
                    </span>
                </p>
                <p class="card-footer-item">
                    <span>
                        Afficher la liste des
                        <a class="has-text-weight-bold" href="./index.php?page=liste&type=categories"
                            >Catégories</a
                        >
                    </span>
                </p>
            </footer>
        </div>
        <div class="card my-5">
            <div class="card-content">
                <p class="title is-4">
                    Ajouter un Favoris ou une Catégorie à ma base de données
                </p>
            </div>
            <footer class="card-footer">
                <p class="card-footer-item">
                    <span>
                        Ajouter un nouveau
                        <a class="has-text-weight-bold" href="./index.php?page=ajouter&type=favori"
                            >Favori</a
                        >
                    </span>
                </p>
                <p class="card-footer-item">
                    <span>
                        Ajouter une nouvelle
                        <a class="has-text-weight-bold" href="./index.php?page=ajouter&type=categorie"
                            >Catégorie</a
                        >
                    </span>
                </p>
            </footer>
        </div>
    </div>
</div>
