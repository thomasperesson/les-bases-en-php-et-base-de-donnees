<?php
$query = Read::getBookmarksAndCategories(true);
$count = $query->rowCount();
?>

<h1 class="title my-6">Liste des favoris <span class="tag is-danger is-medium"><?= $count ?></span>
    <a class="button is-info" href="./index.php?page=ajouter&type=favori">
        <i class="fas fa-plus-circle"></i>&nbsp;Ajouter un Favori
    </a>
</h1>
<table class="table is-bordered is-hoverable is-striped is-fullwidth">
    <thead>
        <tr>
            <th>Nom du favoris</th>
            <th>URL</th>
            <th>Catégorie(s)</th>
            <th>Actions</th>
        </tr>
    <tbody>
        <?php
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($results as $key => $result): ?>
        <?php
            $link_id = $result['id'];
            $link_name = $result['name'];
            $link_url = $result['url'];
            $link_categories = $result['categories'];
        ?>
            <tr>
                <td><?= $link_name ?></td>
                <td><a href="<?= $link_url ?>" target="_blank"><?= $link_url ?></a></td>
                <td><?= $link_categories ?></td>

                <td class="action-column">
                    <div class="columns">
                        <form class="column has-text-centered" action="./crud/delete.php" method="POST">
                            <input type="hidden" name="delete-bookmark" value="<?= $link_id ?>" id="delete-bookmark">
                            <button class="button is-dark is-small">
                                <i class="fas fa-trash"></i>
                            </button>
                        </form>
                        <form class="column has-text-centered" action="./templates/edit-form-bookmark.php" method="POST">
                            <input type="hidden" name="edit-bookmark-id" value="<?= $link_id ?>" id="edit-bookmark-id" />
                            <input type="hidden" name="edit-bookmark-name" value="<?= $link_name ?>" id="edit-bookmark-name" />
                            <input type="hidden" name="edit-bookmark-url" value="<?= $link_url ?>" id="edit-bookmark-url" />
                            <button class="button is-dark is-small">
                                <i class="fas fa-edit"></i>
                            </button>
                        </form>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    </thead>
</table>