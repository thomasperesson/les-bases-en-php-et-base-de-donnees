<h1 class="title">
    Ajouter un nouveau favoris
    <a class="button is-danger is-rounded" href="./index.php?page=liste&type=favoris">
        <i class="fas fa-bookmark"></i>&nbsp;Aller à la liste des favoris
    </a>
</h1>
<div class="forms">
    <div class="box">
        <div class="new-bookmarks-form">
            <form action="./crud/create.php" method="POST">
                <div class="field">
                    <label class="label" for="bookmark-name" require>Nom du favoris*</label>
                    <div class="control">
                        <input class="input" type="text" name="bookmark-name" id="bookmark-name" placeholder="Nom du nouveau favoris" required>
                    </div>
                </div>
                <div class="field">
                    <label class="label" for="bookmark-url" require>URL du favoris*</label>
                    <div class="control">
                        <input class="input" type="text" name="bookmark-url" id="bookmark-url" placeholder="URL du nouveau favoris" autocomplete="off" required>
                    </div>
                </div>
                <div class="block">
                    <div class="field">
                        <label class="label" for="bookmark-categories">Dans quelle catégorie enregistrer ce nouveau lien ?</label>
                        <div class="control">
                            <?php
                            $query = Read::getCategories();
                            $results = $query->fetchAll(PDO::FETCH_ASSOC);
                            foreach ($results as $key => $result) {
                                echo '
                                <label class="checkbox mr-5" for="' . $result['id'] . '">
                                    <input name="id_categories[]" type="checkbox" value="' . $result['id'] . '" id="' . $result['id'] . '" />
                                    ' . $result['name'] . '
                                </label>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <button class="button is-danger" type="submit">Enregistrer</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div> <!-- div.forms -->