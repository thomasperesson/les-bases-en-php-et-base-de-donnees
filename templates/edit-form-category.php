<?php
include './head.php';
?>

<body>
    <?php
    include './navbar.php';
    $id = isset($_POST['edit-category-id']) ? $_POST['edit-category-id'] : null;
    $name = isset($_POST['edit-category-name']) ? $_POST['edit-category-name'] : null;
    $desc = isset($_POST['edit-category-desc']) ? $_POST['edit-category-desc'] : null;

    ?>
    <div class="container">
        <h1 class="title my-6">Page de modification de la catégorie <span class="category-name"><?= $name ?></span></h1>
        <form class="edit-form box" action="../crud/update.php" method="POST">
            <div class="field">
                <label class="label" for="edit-id">ID de la catégorie</label>
                <div class="control">
                    <input class="input is-static is-rounded" type="text" name="edit-id-category" id="edit-id-category" value="<?= $id ?>" readonly>
                </div>
            </div>
            <div class="field">
                <label class="label" for="name">Nom de la Catégorie*</label>
                <div class="control">
                    <input class="input is-rounded" type="text" name="edit-name-category" id="edit-name-category" value="<?= $name ?>">
                </div>
            </div>
            <div class="field">
                <label class="label" for="description">Déscription de la Catégorie</label>
                <div class="control">
                    <textarea class="textarea" name="edit-desc-category" id="edit-desc-category" placeholder="Description de la catégorie"><?= $desc; ?></textarea>
                </div>
            </div>
            <div class="field my-5">
                <div class="control">
                    <button class="button is-primary is-rounded" type="submit">Modifier la catégorie</button>
                </div>
            </div>
        </form>
        <div>
            <a class="button is-danger is-rounded my-6" href="/index.php?page=liste&type=categories">Retour à la liste des catégories</a>
        </div>
    </div>
    <?php include './footer.php'; ?>
</body>

</html>