<h1 class="title">
    Ajouter une nouvelle catégorie
    <a class="button is-danger is-rounded" href="./index.php?page=liste&type=categories">
        <i class="fas fa-file-alt"></i>&nbsp;Aller à la liste des catégories
    </a>
</h1>
<div class="box">
    <div class="new-categories-form">
        <form action="./crud/create.php" method="POST">
            <div class="field">
                <label class="label" for="categorie-name">Nom de la catégorie*</label>
                <div class="control">
                    <input class="input" type="text" name="categorie-name" id="categorie-name" placeholder="Nom de la nouvelle catégorie" autocomplete="off" required>
                </div>
            </div>
            <div class="field">
                <label class="label" for="description">Description de la catégorie</label>
                <div class="control">
                    <textarea class="textarea" name="description" id="description" placeholder="Description de la catégorie"></textarea>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <button class="button is-danger" type="submit">Enregistrer</button>
                </div>
            </div>
        </form>
    </div>
</div>