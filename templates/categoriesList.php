<?php
$query = Read::getCategories();
$count = $query->rowCount();
?>

<h1 class="title my-6">Liste des catégories <span class="tag is-danger is-medium"><?= $count ?></span>
    <a class="button is-info" href="./index.php?page=ajouter&type=categorie">
        <i class="fas fa-plus-circle"></i>&nbsp;Ajouter une Catégorie
    </a>
</h1>
<table class="table is-bordered is-hoverable is-striped is-fullwidth">
    <thead>
        <tr>
            <th>Nom de la catégorie</th>
            <th>Description</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($results as $key => $result): ?>
            <tr>
                <td><?= $result['name'] ?></td>
                <td><?= $result['description'] ?></td>
            
                <td class="action-column">
                    <div class="columns">
                        <form class="column has-text-centered" action="./crud/delete.php" method="POST">
                            <input type="hidden" name="delete-category" value="<?= $result['id'] ?>" id="delete-category">
                            <button class="button is-dark is-small">
                                <i class="fas fa-trash"></i>
                            </button>
                        </form>
                        <form class="column has-text-centered" action="./templates/edit-form-category.php" method="POST">
                            <input type="hidden" name="edit-category-id" value="<?= $result['id'] ?>" id="edit-category-id" />
                            <input type="hidden" name="edit-category-name" value="<?= $result['name'] ?>" id="edit-category-name" />
                            <input type="hidden" name="edit-category-desc" value="<?= $result['description'] ?>" id="edit-category-desc" />
                            <button class="button is-dark is-small">
                                <i class="fas fa-edit"></i>
                            </button>
                        </form>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>