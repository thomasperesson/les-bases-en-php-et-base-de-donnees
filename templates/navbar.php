<header>
    <nav
        class="navbar is-fixed-top is-info"
        role="navigation"
        aria-label="main navigation"
    >
        <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-start">
                <a
                    class="navbar-item"
                    href="./index.php?page=accueil"
                >
                    <i class="fas fa-home"></i>&nbsp;Accueil
                </a>
                <a
                    class="navbar-item"
                    href="./index.php?page=liste&type=favoris"
                >
                    <i class="fas fa-bookmark"></i>&nbsp;Liste des favoris
                </a>
                <a
                    class="navbar-item"
                    href="./index.php?page=liste&type=categories"
                >
                    <i class="fas fa-file-alt"></i>&nbsp;Liste des catégories
                </a>
                <a
                    class="navbar-item"
                    href="./index.php?page=liste&type=categoriesetfavoris"
                >
                    <i class="fas fa-file-alt"></i>&nbsp;Liste des catégories et des favoris
                </a>
                <a
                    class="navbar-item"
                    href="./index.php?page=ajouter&type=favori"
                >
                    <i class="fas fa-plus-circle"></i>&nbsp;Ajouter un Favori
                </a>
                <a
                    class="navbar-item"
                    href="./index.php?page=ajouter&type=categorie"
                >
                    <i class="fas fa-plus-circle"></i>&nbsp;Ajouter une
                    Catégorie
                </a>
            </div>
        </div>
    </nav>
</header>
<div class="container my-6">
    <div class="content">
