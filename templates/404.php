<h1 class="title has-text-centered">Erreur 404 : la page demandée n'existe pas</h1>
<div>
    <a class="button is-danger is-rounded my-6" href="/index.php?page=accueil">Retour à la page d'accueil</a>
</div>