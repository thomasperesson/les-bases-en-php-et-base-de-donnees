-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 13 août 2021 à 13:24
-- Version du serveur : 5.7.33
-- Version de PHP : 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bookmarks`
--

-- --------------------------------------------------------

--
-- Structure de la table `bookmarks`
--

CREATE TABLE `bookmarks` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `url` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `bookmarks`
--

INSERT INTO `bookmarks` (`id`, `name`, `url`) VALUES
(20, 'Facebook', 'https://facebook.com'),
(45, 'Google France', 'https://google.fr'),
(46, 'Google Drive', 'https://drive.google.com/drive/my-drive'),
(47, 'Simplonline', 'https://simplonline.co/login'),
(48, 'CSS Gradient', 'https://cssgradient.io'),
(49, 'Font Awesome', 'https://fontawesome.com'),
(50, 'Marmiton', 'https://www.marmiton.org/'),
(51, 'Selectour', 'https://www.selectour.com/'),
(52, '750g', 'https://www.750g.com/'),
(53, 'Minecraft', 'https://minecraft.org');

-- --------------------------------------------------------

--
-- Structure de la table `bookmarks_categories`
--

CREATE TABLE `bookmarks_categories` (
  `id_bookmarks` int(11) NOT NULL,
  `id_categories` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `bookmarks_categories`
--

INSERT INTO `bookmarks_categories` (`id_bookmarks`, `id_categories`) VALUES
(46, 4),
(48, 1),
(48, 2),
(48, 4),
(49, 1),
(49, 2),
(49, 4),
(20, 7),
(50, 8),
(47, 1),
(47, 2),
(45, 9),
(51, 5),
(52, 8);

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`) VALUES
(1, 'Développement', 'Langages, Frameworks et Librairie'),
(2, 'Formation', 'Campus26 / Simplon.co'),
(3, 'Jeux Vidéo', 'Tous mes sites préférés de jeux vidéo'),
(4, 'Outils', 'Tous les outils liés au développement'),
(5, 'Voyage', 'Tous les liens en relations avec mes futures destinations'),
(7, 'Réseaux Sociaux', 'Tous les sites de mes réseaux sociaux'),
(8, 'Cuisine', 'Marmiton, 750g et compagnie. Tous les mes sites préférés pour la de la cuisine rapide à de la cuisine raffinée.'),
(9, 'Moteur de recherche', 'Rechercher avec un moteur de recherche');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `bookmarks`
--
ALTER TABLE `bookmarks`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bookmarks_categories`
--
ALTER TABLE `bookmarks_categories`
  ADD KEY `id_bookmarks` (`id_bookmarks`),
  ADD KEY `id_categories` (`id_categories`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `bookmarks`
--
ALTER TABLE `bookmarks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `bookmarks_categories`
--
ALTER TABLE `bookmarks_categories`
  ADD CONSTRAINT `bookmarks_categories_ibfk_1` FOREIGN KEY (`id_categories`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `constraint_1` FOREIGN KEY (`id_bookmarks`) REFERENCES `bookmarks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
