<?php
class Connection
{
    public static function dbConnection()
    {
        $hostname = "root";
        $password = "root";
        $connect = new PDO('mysql:host=localhost;dbname=bookmarks;charset=utf8', $hostname, $password);
        return $connect;
    }
}
