<?php
require_once __DIR__ . './Connection.php';
require __DIR__ . './create.php';

/**
 * Mise à jour d'un favoris
 *
 * @param INT $id
 * @param String $name
 * @param String $url
 * @return boolean
 */
function updateBookmark($id, $name, $url)
{
    $db = Connection::dbConnection();
    $update = 'UPDATE bookmarks SET name=:name, url=:url WHERE id=:id';
    $req = $db->prepare($update);
    if ($req->execute(array('id' => $id, 'name' => $name, 'url' => $url))) {
        return true;
    } else {
        return false;
    }
}

/**
 * Mise à jour d'une catégorie
 * 
 * @param INT @id_category
 * @param String $name_category
 * @param String $desc_category
 * @return boolean
 */
function updateCategory($id_category, $name_category, $desc_category)
{
    $db = Connection::dbConnection();
    $updateCategory = "UPDATE categories SET name=:name, description=:description WHERE id=:id_category";
    $req = $db->prepare($updateCategory);
    if ($req->execute(array('id_category' => $id_category, 'name' => $name_category, 'description' => $desc_category))) {
        return true;
    } else {
        return false;
    }
}

/**
 * Met à jour un bookmark en prenant en compte ses catégories
 *
 * @param INT $id_bookmark
 * @param String $name_bookmark
 * @param String $url_bookmark
 * @return boolean
 */
function updateBookmarkWithCategory($id_bookmark, $name_bookmark, $url_bookmark)
{
    $db = Connection::dbConnection();
    // On supprime les lignes lié au bookmark et à ses catégories
    $deleteAssociatedCategories = 'DELETE FROM bookmarks_categories WHERE id_bookmarks=:id_bookmarks';
    $req = $db->prepare($deleteAssociatedCategories);

    if ($req->execute(array('id_bookmarks' => $id_bookmark))) {
        // On met à jour le bookmark
        $updateBookmark = 'UPDATE bookmarks SET name=:name, url=:url WHERE id=:id';
        $req = $db->prepare($updateBookmark);
        if ($req->execute(array('id' => $id_bookmark, 'name' => $name_bookmark, 'url' => $url_bookmark))) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}



if (
    isset($_POST['edit-id-category']) && !empty($_POST['edit-id-category'])
    && isset($_POST['edit-name-category']) && !empty($_POST['edit-name-category'])
    && isset($_POST['edit-desc-category'])
) {
    $id = $_POST['edit-id-category'];
    $name = $_POST['edit-name-category'];
    $desc = $_POST['edit-desc-category'];
    try {
        updateCategory($id, $name, $desc);
    } catch (PDOException $e) {
        die('Erreur :  ' . $e);
    }
    header('Location: ../index.php?page=liste&type=categories');
} else {
    $error = "Une erreur s'est produite";
}

if (
    isset($_POST['edit-id']) && isset($_POST['edit-name']) && isset($_POST['edit-url']) &&
    !empty($_POST['edit-id']) && !empty($_POST['edit-name']) && !empty($_POST['edit-url'])
) {
    $id = $_POST['edit-id'];
    $name = isset($_POST['edit-name']) ? $_POST['edit-name'] : null;
    $url = isset($_POST['edit-url']) ? $_POST['edit-url'] : null;
    try {
        updateBookmarkWithCategory($id, $name, $url);
        if (isset($_POST['id_categories']) && !empty($_POST['id_categories'])) {
            $id_categories = $_POST['id_categories'];
            try {
                foreach ($id_categories as $id_category) {
                    link_category_with_bookmark($id, $id_category);
                }
            } catch (PDOException $e) {
                die('Erreur :  ' . $e);
            }
        }
        header('Location: ../index.php?page=liste&type=favoris');
    } catch (PDOException $e) {
        die('Erreur :  ' . $e);
    }
} else {
    $error = "Une erreur s'est produite";
}