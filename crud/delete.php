<?php
require_once __DIR__ . './Connection.php';

/**
 * Suppression d'un favoris
 *
 * @param INT $id
 * @return boolean
 */
function deleteBookmark($id)
{
    $db = Connection::dbConnection();
    $delete = 'DELETE FROM bookmarks WHERE id=:id';
    $req = $db->prepare($delete);
    if ($req->execute(array('id' => $id))) {
        
        return true;
    } else {
        return false;
    }
}

/**
 * Supprime une catégorie
 * 
 * @param INT $id
 * @return boolean
 */
function deleteCategory($id) {
    $db = Connection::dbConnection();
    $delete = 'DELETE FROM categories WHERE id=:id';
    $req = $db->prepare($delete);
    if($req->execute(array('id' => $id))) { 
        return true;
    } else {
        return false;
    }
}

if (isset($_POST['delete-bookmark'])) {
    $id = $_POST['delete-bookmark'];
    try {
        deleteBookmark($id);
        header('Location: ../index.php?page=liste&type=favoris');
    } catch (PDOException $e) {
        die('Erreur : ' . $e);
    }
} else {
    $error = "Une erreur s'est produite";
}

if(isset($_POST['delete-category']) && !empty($_POST['delete-category'])) {
    $id = $_POST['delete-category'];
    try {
        deleteCategory($id);
        header('Location: ../index.php?page=liste&type=categories');
    } catch(PDOException $e) {
        die('Erreur : ' . $e);
    }
}