<?php
require_once __DIR__ . './Connection.php';

$error = '';

/**
 * Ajoute un nouveau favoris
 *
 * @param String $name
 * @param String $url
 * @return void
 */
function add_bookmark($name, $url)
{
    $db = Connection::dbConnection();
    $insert = "INSERT INTO bookmarks SET name=:name, url=:url";
    $prepare = $db->prepare($insert);
    $prepare->execute(array('name' => $name, 'url' => $url));
    return $db->lastInsertId();
}


/**
 * Ajoute une nouvelle catégorie
 *
 * @param String $name
 * @param String $description
 * @return boolean
 */
function add_category($name, $description)
{
    $db = Connection::dbConnection();
    $insert = "INSERT INTO categories SET name=:name, description=:description";
    $req = $db->prepare($insert);
    if ($req->execute(array('name' => $name, 'description' => $description))) {
        return true;
    } else {
        return false;
    }
}

/**
 * Ajoute les catégories liées au bookmark
 * 
 * @param INT $id_bookmark
 * @param INT $id_category
 * @return boolean
 */
function link_category_with_bookmark($id_bookmark, $id_category)
{
    $db = Connection::dbConnection();
    $insert = "INSERT INTO `bookmarks_categories` (`id_bookmarks`, `id_categories`) VALUES (:id_bookmark, :id_category)";
    $req = $db->prepare($insert);
    if($req->execute(array('id_bookmark' => $id_bookmark, 'id_category' => $id_category))) {
        return true;
    } else {
        return false;
    }
}


if (isset($_POST['bookmark-name']) && isset($_POST['bookmark-url'])) {
    $bookmark_name = $_POST['bookmark-name'];
    $bookmark_url = $_POST['bookmark-url'];
    try {
        $last_insert_id = add_bookmark($bookmark_name, $bookmark_url);
    } catch (PDOException $e) {
        die('Erreur :  ' . $e);
    }
    
    if (isset($_POST['id_categories'])) {
        $db = Connection::dbConnection();
        $id_bookmark = $last_insert_id;
        $id_categories = $_POST['id_categories'];
        try {
            foreach($id_categories as $id_category) {
                link_category_with_bookmark($id_bookmark, $id_category);
            }
            $error = 'OK';
            $error = $GLOBALS['error'];
        } catch (PDOException $e) {
            die('Erreur :  ' . $e);
        }
    }
    header('Location: ../index.php?page=liste&type=favoris');
} else {
    header('Location: ../index.php?page=liste&type=favoris');
}
echo $GLOBALS['error'];

if (isset($_POST['categorie-name']) && isset($_POST['description'])) {
    if (!empty($_POST['categorie-name'])) {
        $name = $_POST['categorie-name'];
        $description = !empty($_POST['description']) ? $_POST['description'] : '';
        try {
            add_category($name, $description);
            header('Location: ../index.php?page=liste&type=categories');
        } catch (PDOException $e) {
            die('Erreur :  ' . $e);
        }
    } else {
        return;
    }
}


