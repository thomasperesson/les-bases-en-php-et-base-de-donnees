<?php
require_once __DIR__ . './Connection.php';

class Read
{

    /**
     * Obtient la liste des Favoris avec les catégories associées
     * Possibilité de concaténer ou non la liste des catégories
     * 
     * @param bool $group_concat
     * @return PDOStatement|false
     */
    public static function getBookmarksAndCategories($group_concat = false)
    {
        if (!$group_concat) {
            $bookmarksAndCategories = "SELECT b.id, b.name, b.url, c.name, c.description FROM bookmarks AS b
            LEFT JOIN bookmarks_categories AS bc ON b.id = bc.id_bookmarks
            LEFT JOIN categories AS c ON c.id = bc.id_categories WHERE id=:id_bookmark ORDER BY b.name ASC;";
            try {
                $db = Connection::dbConnection();
                $query = $db->prepare($bookmarksAndCategories);
                $query->execute();
            } catch (PDOException $e) {
                echo $e;
            }
            return $query;
        } else {
            $group_concat_query = "SELECT b.id , b.name, b.url, GROUP_CONCAT(c.name ORDER BY c.name SEPARATOR ' / ') AS `categories`
                FROM bookmarks AS b
                LEFT JOIN bookmarks_categories AS bc ON bc.id_bookmarks = b.id
                LEFT JOIN categories AS c ON bc.id_categories = c.id
                GROUP BY b.id;";
            try {
                $db = Connection::dbConnection();
                $query = $db->prepare($group_concat_query);
                $query->execute();
            } catch (PDOException $e) {
                echo $e;
            }
            return $query;
        }
    }

    /**
     * Obitent la liste des Favoris (sans lien avec les catégories)
     * 
     * @return PDOStatement|false
     */
    public static function getAllBookmarks()
    {
        $allBookmarks = 'SELECT * FROM bookmarks';
        try {
            $db = Connection::dbConnection();
            $query = $db->prepare($allBookmarks);
            $query->execute();
        } catch (PDOException $e) {
            echo $e;
        }
        return $query;
    }

    /**
     * Obtient la liste des ID des catégories en lien avec un bookmark
     * 
     * @param INT $id_bookmark
     * @return PDOStatement|boolean
     */
    public static function getOneBookmarkWithCategories($id_bookmark, $group_concat = false)
    {
        if (!$group_concat) {
            // On récupère un bookmark et ses catégories associées
            $oneBookmarkWithCategotries = "SELECT c.id, c.name FROM bookmarks_categories AS bc INNER JOIN categories AS c ON c.id = bc.id_categories WHERE id_bookmarks = :id_bookmark";
            try {
                $db = Connection::dbConnection();
                $query = $db->prepare($oneBookmarkWithCategotries);
                $query->execute(array('id_bookmark' => $id_bookmark));
            } catch (PDOException $e) {
                echo $e;
            }
            return $query;
        } else {
            // On récupère un bookmark et ses catégories associées
            $oneBookmarkWithCategotries = "SELECT c.id, GROUP_CONCAT(c.id ORDER BY c.id SEPARATOR ' / ') FROM bookmarks_categories AS bc INNER JOIN categories AS c ON c.id = bc.id_categories WHERE id_bookmarks = :id_bookmark GROUP BY c.id";
            try {
                $db = Connection::dbConnection();
                $query = $db->prepare($oneBookmarkWithCategotries);
                $query->execute(array('id_bookmark' => $id_bookmark));
            } catch (PDOException $e) {
                echo $e;
            }
            return $query;
        }
    }

    /**
     * Obtient la liste des catégories
     * 
     * @return PDOStatement|false
     */
    public static function getCategories()
    {
        $allCategories = "SELECT * FROM `categories` ORDER BY `name` ASC";
        try {
            $db = Connection::dbConnection();
            $query = $db->prepare($allCategories);
            $query->execute();
        } catch (PDOException $e) {
            echo $e;
        }

        return $query;
    }

    /**
     * Obtient la listes des favoris en fonction de l'ID de leur catégorie
     * 
     * @param INT $id_category
     * @return PDOStatement|boolean
     */
    public static function getCategoriesAndHerBookmarks($id_category)
    {
        $bookmarksLinkedToIdCategory =
            "SELECT b.id AS `id`, b.name AS `name`, b.url AS `url` FROM bookmarks AS b
            INNER JOIN bookmarks_categories AS bc ON bc.id_bookmarks = b.id
            INNER JOIN categories AS c ON bc.id_categories = c.id
            WHERE c.id=:id_category";
        try {
            $db = Connection::dbConnection();
            $query = $db->prepare($bookmarksLinkedToIdCategory);
            $query->execute(array('id_category' => $id_category));
        } catch (PDOException $e) {
            echo $e;
        }
        return $query;
    }
}
