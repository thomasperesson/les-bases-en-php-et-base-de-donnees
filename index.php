<?php
require_once "./crud/read.php";

// Head
include './templates/head.php';

// Navbar
include './templates/navbar.php';

// Pages d'accueil (liste des favoris), liste des catégories,
// ajout d'un favoris et d'une catégorie
if(isset($_GET['page'])) {
    switch($_GET['page']) {
        case 'accueil':
            include './templates/home.php';
            break;
        case 'liste':
            if(isset($_GET['type'])) {
                switch($_GET['type']) {
                    case 'favoris':
                        include './templates/bookmarksList.php';
                        break;
                    case 'categories':
                        include './templates/categoriesList.php';
                        break;
                    case 'categoriesetfavoris':
                        include './templates/categoriesAndBookmarksList.php';
                        break;
                    default:
                        include './templates/404.php';
                        break;
                }
            }
            break;
        case 'listecategories':
            include './templates/categoriesList.php';
            break;
        case 'ajouter':
            if(isset($_GET['type'])) {
                switch($_GET['type']) {
                    case 'favori':
                        include './templates/addBookmarkForm.php';
                        break;
                    case 'categorie':
                        include './templates/addCategoryForm.php';
                        break;
                    default:
                        include './templates/404.php';
                        break;
                }
            }
            break;
        default:
            include './templates/404.php';
    }
} else {
    include './templates/home.php';
}

// Footer
include './templates/footer.php'; ?>